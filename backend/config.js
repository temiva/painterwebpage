const fs = require("fs");

let credentials;
exports.get = function () {
    if (!credentials) {
        //Try to read config.json
        const json = fs.readFileSync("./config.json", "UTF-8");
        if (json)
            credentials = JSON.parse(json);
        else {
            console.log("WARNING! You are using dummy development mongo credentials. Specify a config.json to server's root folder!");
            credentials = {
                url: "localhost:27017",
                port: 5000,
                db: "somedb",
                username: "someUser",
                password: "somePassword",
            };
        }
    }
    return credentials;
}