'use strict';
const bcrypt = require('bcrypt')
const mongodb = require("mongodb").MongoClient;
const objectId = require('mongodb').ObjectID;
const config = require("./config.js");

exports.connect = function () {
    return new Promise((resolve, reject) => {
        mongodb.connect(`mongodb://${config.get().username + ":" + config.get().password}@${config.get().url}/${config.get().db}`, {
            useNewUrlParser: true
        }, (err, db) => {
            if (err)
                return reject(err);
            else
                return resolve(db);
        });
    });
}

exports.findUserByEmail = function (email) {
    return new Promise((resolve, reject) => {
        exports.connect()
            .then((db) => {
                db.db(config.get().db).collection("users").find({
                    email: email
                }).toArray((err, res) => {
                    db.close();
                    if (err)
                        return reject(err);
                    else
                        return resolve(res[0]);
                });
            })
            .catch((err) => reject(err));
    });
}

let insertUser = function (user) {
    return new Promise((resolve, reject) => {
        exports.connect()
            .then((db) => {
                db.db(config.get().db).collection("users").insertOne(user, (err, res) => {

                    db.close();
                    if (err)
                        return reject(err);
                    else
                        return resolve("User created");
                });
            })
            .catch((err) => reject(err));
    });
}

exports.createUser = function (email, password) {
    return new Promise((resolve, reject) => {
        if (!email)
            return reject("no email");
        else if (!password)
            return reject("invalid password");
        else if (!email)
            return reject("invalid email");
        else {
            bcrypt.hash(password, 10)
                .then((hash) => {
                    let user = {
                        email: email,
                        hash: hash,
                    }
                    return insertUser(user);
                })
                .then((user) => resolve(user))
                .catch((err) => reject(err));
        }
    });
}

//Parse data that can be send out to the user. Cleanup any sensitive info here
const createSessionUser = function (dbUser) {
    return ({
        user: {
            email: dbUser.email,
            licenses: dbUser.licenses,
        },
        internalId: dbUser._id,
    })
}

exports.login = function (user) {
    return new Promise((resolve, reject) => {
        if (!user.email)
            return reject("no email provided");
        else if (!user.password)
            return reject("no password provided");
        else {
            let loginResponse;
            exports.findUserByEmail(user.email)
                .then((dbUser) => {
                    if (!dbUser)
                        return reject("user not found - " + dbUser + "  " + user.email);
                    else {
                        loginResponse = createSessionUser(dbUser);

                        return dbUser;
                    }
                })
                .then((dbUser) => bcrypt.compare(user.password, dbUser.hash))
                .then(function (passwordsMatch) {
                    if (passwordsMatch === true) {
                        return resolve(loginResponse);
                    } else
                        return reject("wrong password");
                })
                .catch((err) => reject(err));
        }
    });
}

exports.register = function (user) {
    return new Promise((resolve, reject) => {
        if (!user.email)
            return reject("no email provided");
        if (!user.password)
            return reject("no password provided");
        else {
            exports.findUserByEmail(user.email)
                .then((foundUser) => foundUser ? reject("user already exists") : null)
                .then(() => exports.createUser(user.email, user.password))
                .then(() => exports.login(user)) //User created succesfully -> use login to make sure email and password works and to have a valid session data
                .then((user) => resolve(user))
                .catch((err) => {
                    return reject(err)
                });
        }
    });
}

const validCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
const generatorVersion = 1; //Incase we change the algorith but want to still be able to validate old ones.
const generateLicenseKey = function (keyType) {
    let str = "";
    for (let i = 0; i < 25; i++) {
        if (i !== 0 && i % 5 === 0)
            str += "-";
        else
            str += validCharacters.charAt(Math.floor(Math.random() * validCharacters.length));
    }
    const key = {
        value: str,
        version: generatorVersion,
        type: keyType
    }
    return key;
}

const isValidKeyType = function (keyType) {
    return keyType === "commercial" || keyType === "individual" || keyType === "free";
}

exports.getUser = function (userId) {
    return new Promise((resolve, reject) => {

        const selectQuery = {
            _id: new objectId(userId)
        }

        let activedb;
        exports.connect()
            .then((db) => {
                activedb = db;
                return activedb.db(config.get().db).collection("users").findOne(selectQuery);
            })
            .then((user) => {
                if (user)
                    return resolve(user);
                else
                    return reject("user not found");
            })
            .catch((err) => {
                if (activedb)
                    activedb.close();
                return reject(err)
            });
    });
}

exports.createLicense = function (userId, keyType) {
    return new Promise((resolve, reject) => {

        if (!isValidKeyType(keyType))
            return reject("invalid key type");

        let newLicenses;
        let foundUser;
        exports.getUser(userId)
            .then((user) => {
                foundUser = user;
                const licenseKey = generateLicenseKey(keyType);
                const curLicenses = foundUser.licenses ? foundUser.licenses : [];
                newLicenses = [...curLicenses, licenseKey];
                return newLicenses;
            })
            .then((licenses) => {
                return exports.connect();
            })
            .then((db) => {
                let setQuery = {
                    $set: {
                        "licenses": newLicenses,
                    }
                };
                const selectUserQuery = {
                    _id: new objectId(userId)
                }
                db.db(config.get().db).collection("users").updateOne(selectUserQuery, setQuery, (err, res) => {
                    db.close();

                    if (!err) {
                        foundUser.licenses = newLicenses;
                        return resolve(createSessionUser(foundUser));
                    } else
                        return reject(err);
                })
            })
            .catch((err) => {
                return reject(err)
            });
    });
}

exports.setLicenseActivationState = function (userId, licensekey, deviceId, activeDate) {
    return new Promise((resolve, reject) => {
        if (!licensekey)
            return reject("undefined licensekey: " + licensekey);

        let newLicenses;
        let foundUser;
        exports.getUser(userId)
            .then((user) => {
                foundUser = user;
                newLicenses = user.licenses;
                let didUpdate = false;

                if (newLicenses) {
                    newLicenses = newLicenses.map((elem) => {
                        if (elem.value === licensekey) {
                            elem.activationDate = activeDate;
                            elem.deviceId = deviceId;
                            didUpdate = true;
                        }
                        return elem;
                    });
                }

                if (!didUpdate)
                    return reject("license key could not be found");
                else {
                    return newLicenses;
                }
            })
            .then((licenses) => {
                return exports.connect();
            })
            .then((db) => {
                let setQuery = {
                    $set: {
                        "licenses": newLicenses,
                    }
                };
                const selectUserQuery = {
                    _id: new objectId(userId)
                }
                db.db(config.get().db).collection("users").updateOne(selectUserQuery, setQuery, (err, res) => {
                    db.close();
                    if (!err) {
                        foundUser.licenses = newLicenses;
                        return resolve(createSessionUser(foundUser));
                    } else
                        return reject(err);
                })
            })
            .catch((err) => {
                return reject(err)
            });
    });
}