#!/usr/bin/env node

'use strict';
const express = require("express");
const path = require("path");
const database = require("./database.js");
const bodyParser = require('body-parser')
const expressSession = require("express-session")
const config = require("./config.js");

const {
    check,
    validationResult
} = require('express-validator/check');

const app = express();
const publicDir = path.join(__dirname, '../client', 'dist');

app.use(express.static(publicDir));
console.log("Serving from static dir:" + publicDir + " port:" + config.get().port);


//hides the fact that this is all run by express. 
//More info here https://expressjs.com/en/advanced/best-practice-security.html 
//TODO, replace with Helmet
app.disable('x-powered-by');

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(expressSession({
    name: "sessionId",
    resave: false,
    saveUninitialized: false,
    secret: "aspdfjo3i4r30q94tjq3049j2r8u239r82FASDf",
    cookie: {
        maxAge: 15 * 60 * 1000,
        sameSite: true,
        secure: false,
    }
}));

//Middleware handling invalid userId cookies
const validateUserSession = function (request, response, next) {
    //Invalid
    if (!request.session.userId) {
        let msg = {
            error: "session not valid",
        };
        response.json(msg);
    } else {
        next();
    }
}

const createSession = function (session, user, sessionId) {
    if (!sessionId)
        console.log("No session id set");
    session.userId = sessionId;
    session.user = user;
}
const createResponse = function (session, message, error) {
    let msg = {};
    if (session && session.user)
        msg.user = session.user;
    if (error)
        msg.error = error;
    if (message)
        msg.message = message;
    return msg;
}

app.get("/", function (request, response) {
    const indexPath = path.join(publicDir, "index.html");
    response.sendFile(indexPath);
});

app.get("/api/login", function (request, response) {
    let user = {
        email: request.query.email,
        password: request.query.password
    };

    database.login(user)
        .then((loginResponse) => {
            createSession(request.session, loginResponse.user, loginResponse.internalId);
            response.json(createResponse(request.session, "Login succesful", null));
        })
        .catch((err) => {
            console.log(err);
            request.session.destroy((err) => console.log(err));
            response.json(createResponse(null, "Login failed", err));
        });
});

app.get("/api/logout", function (request, response) {
    if (request.session) {
        request.session.destroy((err) => console.log(err));
        const msg = createResponse(request.session, "Logged out", null);
        response.json(msg);
    }
});

app.get("/api/register",
    [
        check('email').escape().isEmail().normalizeEmail(),
        check('password').escape().isLength({
            min: 4
        }),
    ],
    function (request, response) {

        let user = {
            password: request.query.password,
            email: request.query.email
        };

        //Input validation results
        const errors = validationResult(request);
        if (!errors.isEmpty()) {
            response.json(createResponse(request.session, "Failed to register", `${errors.array()[0].param} : ${errors.array()[0].msg}`));
        } else {
            //Register user
            database.register(user)
                .then((loginResponse) => {
                    createSession(request.session, loginResponse.user, loginResponse.internalId);
                    response.json(createResponse(request.session, "User created"));
                })
                .catch((err) => {
                    request.session.destroy((err) => console.log(err));
                    response.json(createResponse(null, "Registering failed", err));
                });
        }
    });

//License purchase
app.get("/api/purchase", validateUserSession, function (request, response) {
    database.createLicense(request.session.userId, request.query.keytype)
        .then((purchasingUser) => {
            createSession(request.session, purchasingUser.user, purchasingUser.internalId);
            response.json(createResponse(request.session, "Purchase complete"));
        })
        .catch((err) => {
            response.json(createResponse(request.session, "Purchase failed", err));
        });
});


//License activation
app.get("/api/license/activate", validateUserSession, function (request, response) {
    database.setLicenseActivationState(request.session.userId, request.query.licensekey, request.query.deviceid, Date())
        .then((userdata) => {
            createSession(request.session, userdata.user, userdata.internalId);
            response.json(createResponse(request.session, "Activation complete"));
        })
        .catch((err) => {
            response.json(createResponse(request.session, "Activation failed", err));
        });
});


//License deactivate
app.get("/api/license/deactivate", validateUserSession, function (request, response) {
    database.setLicenseActivationState(request.session.userId, request.query.licensekey, null, null)
        .then((userdata) => {
            createSession(request.session, userdata.user, userdata.internalId);
            response.json(createResponse(request.session, "Deactivation complete"));
        })
        .catch((err) => {
            response.json(createResponse(request.session, "Deactivation failed", err));
        });
});


app.listen(config.get().port);