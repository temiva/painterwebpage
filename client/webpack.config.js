const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'index_bundle.js'
    },
    module: {
        rules: [
            {
                test:/\.js$/,
                exclude: /node_modules/,
                use: {
                    loader:'babel-loader'
                }
            },
            {
                test: /\.(png|jpg|jpeg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: "assets/"
                        },
                    },
                ], 
            },
            { 
                test: /\.css$/, loader: "style-loader!css-loader" 
            },
        ]
    },
    devServer: {
        port: 8080,
        open: true,
        //Allow express to server from /api during development
        proxy: {
            "/api": "http://localhost:5000"
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        })
    ]
}