/** @format */

'use strict';
import React from 'react';
import ReactDOM from 'react-dom';
import styles from './styling/styles.css';
import {BrowserRouter, Route} from 'react-router-dom';
import {store, OpenSignIn} from './logic/AppState.js';
import {Provider} from 'react-redux';
import {ImageAssets} from './content/Content';
import {Logout} from './logic/APICall.js';

//Components
import Navbar from './components/Navbar.js';
import {SignupModal} from './components/SignupModal.js';
import {ConfirmPurchaseModal} from './components/ConfirmPurchaseModal.js';
import ContentBlur from './components/ContentBlur.js';
import ScrollToTop from './components/ScrollToTop.js';

//Routes
import {RouteOverview} from './logic/routes/RouteOverview.js';
import {RoutePurchase} from './logic/routes/RoutePurchase.js';
import {RouteLearn} from './logic/routes/RouteLearn.js';
import {RouteLicenses} from './logic/routes/RouteLicenses.js';
import {RouteDownload} from './logic/routes/RouteDownload.js';

const navbarLinks = [
	{
		text: 'Login',
		onClick: OpenSignIn,
		className: 'navbar-button-highlighted',
		hideWhenLoggedIn: true,
	},
	{
		text: 'Learn',
		link: '/learn',
		className: 'navbar-button',
	},
	{
		text: 'Purchase',
		link: '/purchase',
		className: 'navbar-button',
	},
	{
		text: 'Overview',
		link: '/',
		className: 'navbar-button',
	},
];
const profilebarLinks = [
	{
		text: 'Logout',
		onClick: Logout,
		className: 'navbar-button-highlighted',
	},
	{
		text: 'Licenses',
		link: '/licenses',
		className: 'navbar-button',
	},
	{
		text: 'Download',
		link: '/download',
		className: 'navbar-button',
	},
];

ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<div>
				<Navbar className="navbar-main" logoImage={ImageAssets.logo} links={navbarLinks} logoLink="/" />
				<Navbar className="navbar-profile" links={profilebarLinks} onlyVisibleWhenLogged="true" />

				<SignupModal />
				<ConfirmPurchaseModal />
				<ScrollToTop>
					<ContentBlur>
						<Route exact path="/" component={RouteOverview} />
						<Route exact path="/learn" component={RouteLearn} />
						<Route exact path="/purchase" component={RoutePurchase} />
						<Route exact path="/licenses" component={RouteLicenses} />
						<Route exact path="/download" component={RouteDownload} />
					</ContentBlur>
				</ScrollToTop>
			</div>
		</BrowserRouter>
	</Provider>,
	document.getElementById('app'),
);
