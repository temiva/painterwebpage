"use strict";
import {
  createStore
} from 'redux'

const initialState = {}
const store = createStore(reducer)

//Actions
const SET_USER = "SET_USER";
const SET_PURCHASEINFO = "SET_PURCHASEINFO";
const OPEN_SIGNIN = "OPEN_SIGNIN";
const OPEN_REGISTER = "OPEN_REGISTER";
const CLOSE_SIGNINMODAL = "CLOSE_SIGNINMODAL";
const SET_WAITINGFORSERVER = "SET_WAITINGFORSERVER";

//Debug
//store.subscribe(() => console.log(store.getState()))

function reducer(state = initialState, action) {
  let newState = Object.assign({}, state);

  if (action.type === OPEN_SIGNIN || action.type === OPEN_REGISTER || action.type === CLOSE_SIGNINMODAL) {
    newState.signupState = action.payload;
  } else if (action.type === SET_PURCHASEINFO) {
    newState.purchaseInfo = action.payload;
  } else if (action.type === SET_USER) {
    newState.user = action.payload.user;
    newState.error = action.payload.error;
    console.log(action.payload.error);
    //Setting a valid user == login, we should hide any signup modals aswell
    if (newState.user) {
      newState.signupState = null;
      newState.purchaseInfo = null;
    }
  } else if (action.type === SET_WAITINGFORSERVER) {
    newState.waitingForServer = action.payload.waitingForServer;
  }
  if (newState.signupState || newState.purchaseInfo)
    newState.blurRootContent = true;
  else
    newState.blurRootContent = false;

  return newState;
}

function SetUser(user, error) {
  const payload = {
    user: user,
    error: error
  };
  store.dispatch({
    type: SET_USER,
    payload: payload
  })
}

function SetWaitingForServer(value) {
  const payload = {
    waitingForServer: value
  };
  store.dispatch({
    type: SET_WAITINGFORSERVER,
    payload: payload
  });
}

function SetPurchaseInfo(info) {
  store.dispatch({
    type: SET_PURCHASEINFO,
    payload: info
  })
}

function OpenSignIn() {
  store.dispatch({
    type: OPEN_SIGNIN,
    payload: "signin"
  })
}

function OpenRegister() {
  store.dispatch({
    type: OPEN_REGISTER,
    payload: "register"
  })
}

function CloseSignInModal() {
  store.dispatch({
    type: CLOSE_SIGNINMODAL,
    payload: null
  })
}

export {
  store,
  SetUser,
  OpenSignIn,
  OpenRegister,
  CloseSignInModal,
  SetPurchaseInfo,
  SetWaitingForServer
}