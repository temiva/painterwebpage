/** @format */

'use strict';
import React, {Component} from 'react';
import styles from '../../styling/overview.css';
import {connect} from 'react-redux';

import ImageCard from '../../components/ImageCard.js';
import BackgroundHole from '../../components/BackgroundHole.js';
import TittleCard from '../../components/TittleCard.js';
import {ImageAssets} from '../../content/Content';
import {OpenRegister} from '../AppState.js';
import {Link} from 'react-router-dom';

function RouteOverviewConnected(props) {
	let tryButton;
	if (props.loggedIn) {
		tryButton = (
			<Link to="/purchase">
				<button className="try-btn">Get Alchemy!</button>
			</Link>
		);
	} else
		tryButton = (
			<button className="try-btn" onClick={OpenRegister}>
				Get Alchemy Studio!
			</button>
		);

	return (
		<div>
			<BackgroundHole image={ImageAssets.overview.backgroundImage} />
			<TittleCard mainText="Alchemy Studio" subText="Art &amp; Artificial intelligence combined!" />
			<ImageCard
				tittle="New way to create art"
				text="Let artificial intelligence aid your creative process"
				image={ImageAssets.overview.image0}
				style="left"
			/>
			<ImageCard
				tittle="Be bold"
				text="Let our magical intelligent brush system help you find new amazing ideas!"
				image={ImageAssets.overview.image1}
				style="right"
			/>
			<ImageCard
				tittle="Be unique"
				text="Alchemy Studio uses procedural generation aid your creative process. Every stroke is quaranteed to be different"
				image={ImageAssets.overview.image2}
				style="left"
			/>
			<div className="main-text">
				<h1>Ready to be different?</h1>
				{tryButton}
				<p>By the way... It's free :)</p>
			</div>
		</div>
	);
}

const stateToProps = function(state) {
	return {
		loggedIn: state.user ? true : false,
	};
};

//Component connections to Redux state
const RouteOverview = connect(stateToProps)(RouteOverviewConnected);

export {RouteOverview};
