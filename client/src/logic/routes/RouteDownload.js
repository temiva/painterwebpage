/** @format */

'use strict';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router';

function RouteDownloadConnected(props) {
	if (props.loggedIn) {
		return (
			<div className="main-text">
				<h3>Download</h3>
				<p>This is a demo so there is nothing to download :-(</p>
			</div>
		);
	} else return <Redirect to="/" />;
}

const stateToProps = function(state) {
	return {loggedIn: state.user ? true : false};
};
const RouteDownload = connect(stateToProps)(RouteDownloadConnected);

export {RouteDownload};
