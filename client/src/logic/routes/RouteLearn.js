/** @format */

'use strict';
import React, {Component, Fragment} from 'react';
import TutorialCard from '../../components/TutorialCard.js';
import styles from '../../styling/learn.css';
import {VideoTutorials} from '../../content/Content.js';
import {ImageAssets} from '../../content/Content';
import BackgroundHole from '../../components/BackgroundHole.js';

function RouteLearn(props) {
	let cards = VideoTutorials.map((tutorial, index) => {
		const style = index % 2 === 0 ? 'left' : 'right';
		return (
			<div key={index}>
				<TutorialCard
					mainText={tutorial.title}
					subText={tutorial.subText}
					description={tutorial.description}
					videoLink={tutorial.videoUrl}
					style={style}
				/>
			</div>
		);
	});
	return (
		<Fragment>
			<BackgroundHole image={ImageAssets.learn.backgroundImage} />
			<div className="tutorial-container">
				<h1>Tutorials</h1>
				<p>Just getting started with Alchemy? We got you covered!</p>
				{cards}
			</div>
		</Fragment>
	);
}

export {RouteLearn};
