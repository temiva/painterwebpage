/** @format */

'use strict';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {OpenRegister, OpenSignIn, SetPurchaseInfo} from '../AppState.js';
import {LicenseInfo} from '../../content/Content.js';
import styles from '../../styling/purchase.css';

function TableActionElement(props) {
	let elem = (
		<div>
			<h2 className="table-product">{props.mainText}</h2>
			<p className="table-cost">{props.costText}</p>
			<button className="table-purchase-button" onClick={props.onClick}>
				Purchase
			</button>
		</div>
	);
	return elem;
}

function RoutePurchaseConnected(props) {
	let productInfo = (
		<div className="table-container">
			<table>
				<tbody>
					<tr>
						<td className="table-transparent-field" />
						<td className="table-main-field background0">
							<TableActionElement
								mainText={LicenseInfo.commercial.name}
								costText={`${LicenseInfo.commercial.cost}$/year`}
								onClick={props.loggedIn ? () => SetPurchaseInfo(LicenseInfo.commercial) : OpenSignIn}
							/>
						</td>
						<td className="table-main-field background1">
							<TableActionElement
								mainText={LicenseInfo.individual.name}
								costText={`${LicenseInfo.individual.cost}$/year`}
								onClick={props.loggedIn ? () => SetPurchaseInfo(LicenseInfo.individual) : OpenSignIn}
							/>
						</td>
						<td className="table-main-field background2">
							<TableActionElement
								mainText={LicenseInfo.free.name}
								costText="for 30 days"
								link="/register"
								onClick={props.loggedIn ? () => SetPurchaseInfo(LicenseInfo.free) : OpenSignIn}
							/>
						</td>
					</tr>
					<tr>
						<td className="table-transparent-field">Max image size</td>
						<td className="background0-field">16 000 pixels</td>
						<td className="background1-field">16 000 pixels</td>
						<td className="background2-field">2048 pixels</td>
					</tr>
					<tr>
						<td className="table-transparent-field">Watermark</td>
						<td className="background0-field">No</td>
						<td className="background1-field">No</td>
						<td className="background2-field">Yes</td>
					</tr>
					<tr>
						<td className="table-transparent-field">Annual revenue limit</td>
						<td className="background0-field">above 50 000$</td>
						<td className="background1-field">below 50 000$</td>
						<td className="background2-field">No limit</td>
					</tr>
				</tbody>
			</table>
		</div>
	);

	return (
		<div>
			<div className="purchase-text">
				<h2>Ready to get started?</h2>
				<p>Select your plan</p>
			</div>
			{productInfo}
		</div>
	);
}

const stateToProps = function(state) {
	return {loggedIn: state.user ? true : false};
};
const RoutePurchase = connect(stateToProps)(RoutePurchaseConnected);

export {RoutePurchase};
