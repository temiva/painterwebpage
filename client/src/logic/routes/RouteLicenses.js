/** @format */

'use strict';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router';
import {ActivateLicense, DeactivateLicense} from '../APICall.js';
import {Link} from 'react-router-dom';
import styles from '../../styling/licenses.css';

function RouteLicensesConnected(props) {
	if (props.loggedIn) {
		let tableElements;
		if (props.user && props.user.licenses) {
			tableElements = props.user.licenses.map((license, index) => {
				let statusField;
				let deviceId;
				let activationField;
				let demoActivateField;
				if (license.activationDate) {
					const date = new Date(license.activationDate);
					statusField = `${date.getDate()}.${date.getMonth()}.${date.getFullYear()}`;
					deviceId = license.deviceId;
					activationField = (
						<button className="license-deactivate-button" onClick={() => DeactivateLicense(license.value)}>
							Deactivate
						</button>
					);
					demoActivateField = null;
				} else {
					deviceId = 'Ready to activate';
					demoActivateField = (
						<button className="license-activate-button" onClick={() => ActivateLicense(license.value)}>
							Demo activate
						</button>
					);
				}
				return (
					<tr key={index}>
						<td>{license.value}</td>
						<td>{license.type}</td>
						<td>
							<div>{deviceId}</div>
							<div>{statusField}</div>
						</td>
						<td>{activationField}</td>
						<td>{demoActivateField}</td>
					</tr>
				);
			});
		}

		//License content did exist -> show table of licenses
		if (tableElements) {
			const licenseContent = (
				<table>
					<tbody>
						<tr>
							<th>License key</th>
							<th>Type</th>
							<th>Status</th>
							<th />
						</tr>
						{tableElements}
					</tbody>
				</table>
			);
			return (
				<div className="main-text">
					<h3>Your licenses</h3>
					<div>{licenseContent}</div>
				</div>
			);
		}
		//There was no licenses -> show button to purhcase a license
		else {
			return (
				<div className="main-text">
					<h3>You haven't purchased any licenses yet</h3>
					<Link to="/purchase">
						<button className="try-btn">Purchase Alchemy Studio!</button>
					</Link>
				</div>
			);
		}
	} else return <Redirect to="/" />;
}

const stateToProps = function(state) {
	return {
		loggedIn: state.user ? true : false,
		user: state.user,
	};
};

//Component connections to Redux state
const RouteLicenses = connect(stateToProps)(RouteLicensesConnected);

export {RouteLicenses};
