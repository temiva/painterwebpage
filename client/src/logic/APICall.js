/** @format */

'use strict';
import {
  SetUser,
  SetWaitingForServer
} from './AppState.js';

const REGISTER_URL = '/api/register';
const LOGIN_URL = '/api/login';
const LOGOUT_URL = '/api/logout';
const PURCHASE_URL = '/api/purchase';
const ACTIVATELICENSE_URL = '/api/license/activate';
const DEACTIVATELICENSE_URL = '/api/license/deactivate';

function fetchFromApi(urlStr, parametersStr) {
  const reqUrl = `${urlStr}?${parametersStr}`;
  return Promise.race([
    //Fetch
    new Promise((resolve, reject) => {
      fetch(reqUrl)
        .then(function (response) {
          if (response.ok) return response.json();
          else {
            return reject(response.statusText);
          }
        })
        .then(function (json) {
          return resolve(json);
        })
        .catch(() => {
          return reject('Network error. Failed to connect to server');
        });
    }),
    //Timeout if fetch doesn't seem to complete
    new Promise((resolve, reject) => {
      return setTimeout(() => {
        reject('Timeout. Server is not responding');
      }, 5000);
    }),
  ]);
}

function Register(email, password) {
  const paramsUrl = `email=${email}&password=${password}`;
  SetWaitingForServer(true);

  fetchFromApi(REGISTER_URL, paramsUrl)
    .then(json => {
      SetWaitingForServer(false);
      SetUser(json.user, json.error);
    })
    .catch(err => {
      SetWaitingForServer(false);
      SetUser(null, err);
    });
}

function Login(email, password) {
  const paramsUrl = `email=${email}&password=${password}`;
  SetWaitingForServer(true);

  fetchFromApi(LOGIN_URL, paramsUrl)
    .then(json => {
      SetWaitingForServer(false);
      SetUser(json.user, json.error);
    })
    .catch(err => {
      SetWaitingForServer(false);
      SetUser(null, err);
    });
}

function Logout() {
  fetchFromApi(LOGOUT_URL).then(json => {
    SetUser(json.user, json.error);
  });
}

function PurchaseLicense(licenseType) {
  const paramsUrl = `keytype=${licenseType}`;
  fetchFromApi(PURCHASE_URL, paramsUrl).then(json => {
    SetUser(json.user, json.error);
  });
}

function ActivateLicense(licenseKey, deviceId = 'demo-device') {
  const paramsUrl = `licensekey=${licenseKey}&deviceid=${deviceId}`;
  fetchFromApi(ACTIVATELICENSE_URL, paramsUrl).then(json => {
    SetUser(json.user, json.error);
  });
}

function DeactivateLicense(licenseKey) {
  const paramsUrl = `licensekey=${licenseKey}`;
  fetchFromApi(DEACTIVATELICENSE_URL, paramsUrl).then(json => {
    SetUser(json.user, json.error);
  });
}

export {
  Register,
  Login,
  Logout,
  PurchaseLicense,
  ActivateLicense,
  DeactivateLicense
};