/** @format */

'use strict';
import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import styles from '../styling/navbar.css';

const stateToProps = function(state) {
	return {loggedIn: state.user ? true : false};
};

function NavbarConnected(props) {
	const isVisible = !props.onlyVisibleWhenLogged || (props.onlyVisibleWhenLogged && props.loggedIn);

	let logo;
	if (props.logoImage) {
		logo = (
			<Link to={props.logoLink}>
				<img src={props.logoImage} className="navbar-logo" />
			</Link>
		);
	}

	const buttons = props.links.map((elem, index) => {
		//Don't render if hideWhenLoggedIn flag is invalid
		if (props.loggedIn === false || !elem.hideWhenLoggedIn) {
			const isLink = elem.link;
			const button = (
				<button className={elem.className} onClick={elem.onClick} key={index}>
					{elem.text}
				</button>
			);

			if (isLink)
				return (
					<Link to={elem.link} key={index}>
						{button}
					</Link>
				);
			else return button;
		}
	});

	if (isVisible) {
		return (
			<nav className={props.className}>
				{logo}
				{buttons}
			</nav>
		);
	} else {
		return <nav />;
	}
}

const Navbar = connect(stateToProps)(NavbarConnected);
export default Navbar;
