/** @format */

'use strict';
import React from 'react';

function BackgroundHole(props) {
	const style = {backgroundImage: 'url(' + props.image + ')'};

	return <div style={style} className="background-hole" />;
}

export default BackgroundHole;
