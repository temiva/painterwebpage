/** @format */

'use strict';
import React from 'react';

function TutorialCard(props) {
	const video = (
		<div className="video-container">
			<iframe
				className="video-iframe"
				src={props.videoLink}
				frameBorder="0"
				allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
				allowFullScreen
			/>
		</div>
	);
	const description = (
		<div className="video-description-container">
			<h3 className="tutorial-title">{props.mainText}</h3>
			<p className="sub-text">{props.subText}</p>
			<p className="sub-description">{props.description}</p>
		</div>
	);
	if (props.style === 'left') {
		return (
			<div className="tutorial-card">
				{description}
				{video}
			</div>
		);
	} else {
		return (
			<div className="tutorial-card">
				{video}
				{description}
			</div>
		);
	}
}

export default TutorialCard;
