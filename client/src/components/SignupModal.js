/** @format */

'use strict';
import React from 'react';
import {connect} from 'react-redux';
import {CloseSignInModal, OpenSignIn, OpenRegister} from '../logic/AppState.js';
import LoginForm from './LoginForm.js';
import RegisterForm from './RegisterForm.js';
import styles from '../styling/forms.css';

const stateToProps = function(state) {
	return {
		error: state.error ? state.error : null,
		signupState: state.signupState,
	};
};

function SignupModalConnected(props) {
	if (props.signupState) {
		let form;
		let registerTabClass;
		let loginTabClass;

		if (props.signupState === 'signin') {
			form = <LoginForm error={props.error} />;
			loginTabClass = 'signup-tab';
			registerTabClass = 'signup-tab signup-tab-deactive';
		} else if (props.signupState === 'register') {
			form = <RegisterForm error={props.error} />;
			registerTabClass = 'signup-tab';
			loginTabClass = 'signup-tab signup-tab-deactive';
		}

		//Move modal view to current scroll position
		const style = {
			top: document.documentElement.scrollTop + window.innerHeight * 0.5,
		};
		return (
			<div>
				<div className="screen-dimmer" onClick={CloseSignInModal} />

				<div className="modal signup-container" style={style}>
					<div className="signup-tabs-container">
						<button className={loginTabClass} onClick={OpenSignIn}>
							Sign in
						</button>
						<button className={registerTabClass} onClick={OpenRegister}>
							Register
						</button>
					</div>
					{form}
				</div>
			</div>
		);
	}
	return null;
}

const SignupModal = connect(stateToProps)(SignupModalConnected);
export {SignupModal};
