/** @format */

'use strict';
import React from 'react';
import {connect} from 'react-redux';
import {Login as LoginUser} from '../logic/APICall.js';
import ServerStatusIndicator from './ServerStatusIndicator.js';
import styles from '../styling/forms.css';

function handleSubmit(e) {
	const emailElem = document.getElementById('login-email');
	const passwordElem = document.getElementById('login-password');
	LoginUser(emailElem.value, passwordElem.value);
}

const stateToProps = function(state) {
	return {
		error: state.error,
		waitingForServer: state.waitingForServer,
	};
};

function LoginFormConnected(props) {
	return (
		<div className="form-container">
			<ServerStatusIndicator waitingForServer={props.waitingForServer} error={props.error} />
			<div className="input-container">
				<input type="text" name="email" id="login-email" placeholder="e-mail" defaultValue="demouser@demo.com" />
			</div>
			<br />
			<div className="input-container">
				<input type="password" name="password" id="login-password" placeholder="password" defaultValue="demouser" />
			</div>
			<br />
			<div className="form-button-container">
				<button className="form-button" onClick={handleSubmit}>
					Sign in
				</button>
			</div>
		</div>
	);
}

const LoginForm = connect(stateToProps)(LoginFormConnected);
export default LoginForm;
