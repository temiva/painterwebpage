/** @format */

'use strict';
import React from 'react';
import {Fragment} from 'react';

function ServerStatusIndicator(props) {
	//Show loading animation when waiting for server
	if (props.waitingForServer) {
		return (
			<Fragment>
				<div className="loader" />
			</Fragment>
		);
	}
	//Show error once server has responded
	else {
		return (
			<Fragment>
				<div className="form-error">{props.error}</div>
			</Fragment>
		);
	}
}
export default ServerStatusIndicator;
