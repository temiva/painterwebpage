/** @format */

'use strict';
import React from 'react';
import {connect} from 'react-redux';
import {Register as RegisterUser} from '../logic/APICall.js';
import ServerStatusIndicator from './ServerStatusIndicator.js';
import styles from '../styling/forms.css';

function handleSubmit(e) {
	const emailElem = document.getElementById('register-email');
	const passwordElem = document.getElementById('register-password');
	RegisterUser(emailElem.value, passwordElem.value);
}

const stateToProps = function(state) {
	return {
		error: state.error,
		waitingForServer: state.waitingForServer,
	};
};

function RegisterFormConnected(props) {
	return (
		<div className="form-container">
			<ServerStatusIndicator waitingForServer={props.waitingForServer} error={props.error} />
			<div className="form-note">This is a demo. Please don't use your real e-mail</div>
			<div className="input-container">
				<input type="text" name="email" id="register-email" placeholder="e-mail" /*defaultValue="demouser@demo.com"*/ />
			</div>
			<br />
			<div className="input-container">
				<input
					type="password"
					name="password"
					id="register-password"
					placeholder="password" /*defaultValue="demouser"*/
				/>
			</div>
			<br />
			<div className="form-button-container">
				<button className="form-button" onClick={handleSubmit}>
					Register
				</button>
			</div>
		</div>
	);
}
const RegisterForm = connect(stateToProps)(RegisterFormConnected);

export default RegisterForm;
