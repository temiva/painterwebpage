/** @format */

'use strict';
import React from 'react';

function TittleCard(props) {
	return (
		<div className="tittle-card">
			<h1>{props.mainText}</h1>
			<h3>{props.subText}</h3>
		</div>
	);
}

export default TittleCard;
