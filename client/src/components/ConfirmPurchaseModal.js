/** @format */

'use strict';
import React from 'react';
import {connect} from 'react-redux';
import {SetPurchaseInfo} from '../logic/AppState.js';
import {PurchaseLicense} from '../logic/APICall.js';
import styles from '../styling/confirm-purchase.css';

const stateToProps = function(state) {
	return {
		purchaseInfo: state.purchaseInfo,
	};
};

const closeModal = function() {
	SetPurchaseInfo(null);
};

function ConfirmPurchaseModalConnected(props) {
	if (props.purchaseInfo) {
		//Move modal view to current scroll position
		const style = {
			top: document.documentElement.scrollTop + window.innerHeight * 0.5,
		};
		const cost = (
			<div>
				<span className="confirm-cost">{props.purchaseInfo.cost !== 0 ? props.purchaseInfo.cost + '$' : 'free'}</span>
				<span className="confirm-cost-subtext">{props.purchaseInfo.cost !== 0 ? ' per year' : ''}</span>
			</div>
		);

		return (
			<div>
				<div className="screen-dimmer" onClick={closeModal} />

				<div className="modal confirm-purchase-container" style={style}>
					<h3 className="confirm-header">{props.purchaseInfo.name} license</h3>
					<p className="confirm-note">"NOTE! This is a demo purchase."</p>
					<p className="confirm-description">{props.purchaseInfo.description}</p>
					{cost}
					<div className="confirm-buttons-container">
						<button onClick={() => PurchaseLicense(props.purchaseInfo.APITypeName)} className="corfirm-button">
							Purchase
						</button>
						<button onClick={closeModal} className="corfirm-button corfirm-button-cancel">
							Cancel
						</button>
					</div>
				</div>
			</div>
		);
	}
	return null;
}

const ConfirmPurchaseModal = connect(stateToProps)(ConfirmPurchaseModalConnected);
export {ConfirmPurchaseModal};
