/** @format */

'use strict';
import React from 'react';

function ImageCard(props) {
	const style = {backgroundImage: 'url(' + props.image + ')'};
	let textDiv = (
		<div className="image-card-content image-card-text">
			<h2>{props.tittle}</h2>
			<p>{props.text}</p>
		</div>
	);
	let imageDiv = <div style={style} className="image-card-content image-card-image" />;
	let divs;
	if (props.style == 'left') {
		divs = (
			<div className="image-card">
				{textDiv}
				{imageDiv}
			</div>
		);
	} else {
		divs = (
			<div className="image-card">
				{imageDiv}
				{textDiv}
			</div>
		);
	}

	return divs;
}

export default ImageCard;
