/** @format */

'use strict';
import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

const stateToProps = function(state) {
	return {blur: state.blurRootContent};
};

function ContentBlurConnected(props) {
	return (
		<div id="root-content" style={props.blur ? {filter: 'blur(3px) brightness(80%)'} : {filter: ''}}>
			{props.children}
		</div>
	);
}

const ContentBlur = withRouter(connect(stateToProps)(ContentBlurConnected));
export default ContentBlur;
