import img0 from "../static/assets/yellow-metal-design-decoration.jpg";
import img1 from "../static/assets/pexels-photo-935785.jpeg";
import img2 from "../static/assets/test2.jpeg";
import imgLearnBG from "../static/assets/pexels-photo.jpg";
import logo from "../static/assets/logo.png";

import backgroundImage from "../static/assets/pexels-photo-1209843.jpeg"; //"../static/assets/test1.jpeg";

export const ImageAssets = {
    logo: logo,
    overview: {
        backgroundImage: backgroundImage,
        image0: img0,
        image1: img1,
        image2: img2,
    },
    learn: {
        backgroundImage: imgLearnBG,
    },
}

export const LicenseInfo = {
    commercial: {
        name: "Commercial",
        APITypeName: "commercial",
        cost: 199,
        description: "Commercial license is for legal entities with annual revenue above 50,000$"
    },
    individual: {
        name: "Individual",
        APITypeName: "individual",
        cost: 60,
        description: "Commercial license is for legal entities with annual revenue below 50,000$. If your revenue is above 50,000$ then you'll have to purchase the commercial license instead",
    },
    free: {
        name: "Free",
        APITypeName: "free",
        description: "Free version can be used for 30 days without any limits. After the 30 day period output image size is limited to 2048 pixels. Saved images have a small watermark on the lower left corner",
        cost: 0,
    }
}

export const VideoTutorials = [{
        title: "Basic painting",
        subText: "Beginner",
        videoUrl: "https://www.youtube.com/embed/LU7cL7KySSE",
        description: "Learn how to paint using regular brushes",
    },
    {
        title: "Brush parameters",
        subText: "Beginner",
        videoUrl: "https://www.youtube.com/embed/-Nt9fa8jZUE",
        description: "Learn how to adjust brush parameters",
    },
    {
        title: "Photo to painting transformation",
        subText: "Advanced",
        videoUrl: "https://www.youtube.com/embed/Oo8IUxOj4DM",
        description: "Advanced tutorial on transforming a photo into a realistic painting",
    },
    {
        title: "Photorealistic painting manipulation",
        subText: "Advanced",
        videoUrl: "https://www.youtube.com/embed/fqGba-Yw4KY",
        description: "Convert photo to a photorealistic painting",
    },
]