**This is a demo project of a desktop application's license manager**

The application is imaginary painting app (like Photoshop or Corel Painter etc.) which would require a license management system to bind pc device to a license key.
LIVE DEMO -> http://139.59.159.30:8080/

* Users can register and login to the site
* Purchase licenses of an imaginary painting desktop application
* Activate and deactivate their licenses to a device. (demo has a button to activate on a imaginary device id)

In real project the activation would be done by the desktop app itself. Then users would be able to release licenses via the web interface.
Serverside handles registering new users, signing them in, session management and license generation / activation / deactivation via a basic API layer.

All images are from https://www.pexels.com/

Technologies used:

**Front-end**

| Technology |  |
| ------ | ------ |
| React | Single page app |
| Redux | Handles state | 
| Babel | legacy support | 
| Webpack | build manager | 


**Back-end**

| Technology |  |
| ------ | ------ |
| Express | server |
| Express-session | session manager | 
| MongoDB | database | 
| Bcrypt | password hashing | 
